<?php

// Movie Titles start Q3
$titles = [];
$page = 0;
do {
    $url = 'https://jsonmock.hackerrank.com/api/movies/search/?Title=spiderman&page='. ++$page;
    $contents = file_get_contents($url);

    $json = json_decode($contents, true);

    if(count($json['data'])) {
        foreach ($json['data'] as $datum) {
            $titles[$datum['imdbID']] = $datum['Title'];
        }
    }

} while (count($json['data']));

sort($titles);
print_r($titles);
exit;