<?php

// Braces start Q2
$possibleStart = [
    'square'      => '[',
    'curly'       => '{',
    'parentheses' => '('
];

$possibleEnd = [
    'square'      => ']',
    'curly'       => '}',
    'parentheses' => ')'
];

$rows = [
    '{}', // YES
    '[]', // YES
    '()', // YES
    '}{', // NO
    '}}', // NO
    '))', // NO
    ')(', // NO
    '[[', // NO
    '][', // NO
    '[{', // NO
    '[{}]', // YES
    '[}]{}', // NO
    '{}[]()', // YES
    '{[}]}', // NO
];

$result = [];
foreach ($rows as $rowKey => $row) {
    $result[$rowKey] = 'YES';

    $chars = str_split($row);

    $temp = [];

    $temp['square']['pair']      = 0;
    $temp['curly']['pair']       = 0;
    $temp['parentheses']['pair'] = 0;

    foreach ($chars as $char) {

        if (in_array($char, $possibleStart)) {
            $pair = $temp[array_search($char, $possibleStart)]['pair'] += 1;
            $temp[array_search($char, $possibleStart)][$pair]['start'] = true;
        }


        if (in_array($char, $possibleEnd)) {
            $pair = $temp[array_search($char, $possibleEnd)]['pair'];
            if (!empty($temp[array_search($char, $possibleEnd)][$pair]['start'])) {
                $temp[array_search($char, $possibleEnd)][$pair]['end'] = true;
                $temp[array_search($char, $possibleEnd)]['pair']++;
            } else {
                $pair = $temp[array_search($char, $possibleEnd)]['pair']++;
                $temp[array_search($char, $possibleEnd)][$pair]['end'] = true;
            }
        }
    }

    // Check for all invalid results
    foreach ($temp as $res) {
        unset($res['pair']);
        foreach ($res as $pair) {
            if (count($pair) != 2) {
                $result[$rowKey] = 'NO';
            }
        }
    }
}

print_r($result);
exit;