<?php

function numOfPlaylists(int $n, int $l, int $k = 1)
{
    $result = 1;

    while($l--) {
        $result *= $n--;
    }

    return $result;
}

$test1 = numOfPlaylists(3, 3); // answer 6
$test2 = numOfPlaylists(1, 3); // answer 0

var_dump($test1, $test2);exit;