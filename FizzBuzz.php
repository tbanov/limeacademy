<?php

for ($i = 1; $i <= 50; $i++) {

    if ($i % 3 == 0 && $i % 5 == 0) {
        echo $i . " % 3 and 5 (FizzBuzz)" . PHP_EOL;
    } elseif ($i % 3 == 0) {
        echo $i . " % 3 (Fizz)" . PHP_EOL;
    } elseif ($i % 5 == 0) {
        echo $i . " % 5 (Buzz)" . PHP_EOL;
    } else {
        echo $i . PHP_EOL;
    }
}
exit;